/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.catalogmanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.CatalogEntryPackage;
import org.kathra.core.model.CatalogEntryPackageVersion;

public class ReadCatalogEntriesClient {
    private ApiClient apiClient;

    public ReadCatalogEntriesClient() {
        this.apiClient = new ApiClient().setUserAgent("ReadCatalogEntriesClient 1.2.0");
    }

    public ReadCatalogEntriesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public ReadCatalogEntriesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for getAllCatalogEntryPackages
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getAllCatalogEntryPackagesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogEntries";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getAllCatalogEntryPackagesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getAllCatalogEntryPackagesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get all entries in the catalog
     * 
     * @return List&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<CatalogEntryPackage> getAllCatalogEntryPackages() throws ApiException {
        ApiResponse<List<CatalogEntryPackage>> resp = getAllCatalogEntryPackagesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Get all entries in the catalog
     * 
     * @return ApiResponse&lt;List&lt;CatalogEntryPackage&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<CatalogEntryPackage>> getAllCatalogEntryPackagesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getAllCatalogEntryPackagesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackage>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get all entries in the catalog (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getAllCatalogEntryPackagesAsync(final ApiCallback<List<CatalogEntryPackage>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getAllCatalogEntryPackagesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackage>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntryFromVersion
     * @param providerId CatalogEntryPackage providerId (required)
     * @param version CatalogEntryPackage version (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryFromVersionCall(String providerId, String version, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogEntries/{providerId}/versions/{version}"
            .replaceAll("\\{" + "providerId" + "\\}", apiClient.escapeString(providerId.toString()))
            .replaceAll("\\{" + "version" + "\\}", apiClient.escapeString(version.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryFromVersionValidateBeforeCall(String providerId, String version, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'providerId' is set
        if (providerId == null) {
            throw new ApiException("Missing the required parameter 'providerId' when calling getCatalogEntryFromVersion(Async)");
        }
        
        // verify the required parameter 'version' is set
        if (version == null) {
            throw new ApiException("Missing the required parameter 'version' when calling getCatalogEntryFromVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCatalogEntryFromVersionCall(providerId, version, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get an entry in the catalog for specific version
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @param version CatalogEntryPackage version (required)
     * @return CatalogEntryPackageVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackageVersion getCatalogEntryFromVersion(String providerId, String version) throws ApiException {
        ApiResponse<CatalogEntryPackageVersion> resp = getCatalogEntryFromVersionWithHttpInfo(providerId, version);
        return resp.getData();
    }

    /**
     * Get an entry in the catalog for specific version
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @param version CatalogEntryPackage version (required)
     * @return ApiResponse&lt;CatalogEntryPackageVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackageVersion> getCatalogEntryFromVersionWithHttpInfo(String providerId, String version) throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryFromVersionValidateBeforeCall(providerId, version, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackageVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get an entry in the catalog for specific version (asynchronously)
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @param version CatalogEntryPackage version (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryFromVersionAsync(String providerId, String version, final ApiCallback<CatalogEntryPackageVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryFromVersionValidateBeforeCall(providerId, version, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackageVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntryPackage
     * @param providerId CatalogEntryPackage providerId (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageCall(String providerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogEntries/{providerId}"
            .replaceAll("\\{" + "providerId" + "\\}", apiClient.escapeString(providerId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryPackageValidateBeforeCall(String providerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'providerId' is set
        if (providerId == null) {
            throw new ApiException("Missing the required parameter 'providerId' when calling getCatalogEntryPackage(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCatalogEntryPackageCall(providerId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get an entry in the catalog
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @return CatalogEntryPackage
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackage getCatalogEntryPackage(String providerId) throws ApiException {
        ApiResponse<CatalogEntryPackage> resp = getCatalogEntryPackageWithHttpInfo(providerId);
        return resp.getData();
    }

    /**
     * Get an entry in the catalog
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @return ApiResponse&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackage> getCatalogEntryPackageWithHttpInfo(String providerId) throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryPackageValidateBeforeCall(providerId, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get an entry in the catalog (asynchronously)
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageAsync(String providerId, final ApiCallback<CatalogEntryPackage> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryPackageValidateBeforeCall(providerId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntryPackageVersions
     * @param providerId CatalogEntryPackage providerId (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageVersionsCall(String providerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogEntries/{providerId}/versions"
            .replaceAll("\\{" + "providerId" + "\\}", apiClient.escapeString(providerId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryPackageVersionsValidateBeforeCall(String providerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'providerId' is set
        if (providerId == null) {
            throw new ApiException("Missing the required parameter 'providerId' when calling getCatalogEntryPackageVersions(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCatalogEntryPackageVersionsCall(providerId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get all version for an entry in the catalog
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @return List&lt;CatalogEntryPackageVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<CatalogEntryPackageVersion> getCatalogEntryPackageVersions(String providerId) throws ApiException {
        ApiResponse<List<CatalogEntryPackageVersion>> resp = getCatalogEntryPackageVersionsWithHttpInfo(providerId);
        return resp.getData();
    }

    /**
     * Get all version for an entry in the catalog
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @return ApiResponse&lt;List&lt;CatalogEntryPackageVersion&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<CatalogEntryPackageVersion>> getCatalogEntryPackageVersionsWithHttpInfo(String providerId) throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryPackageVersionsValidateBeforeCall(providerId, null, null);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackageVersion>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get all version for an entry in the catalog (asynchronously)
     * 
     * @param providerId CatalogEntryPackage providerId (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageVersionsAsync(String providerId, final ApiCallback<List<CatalogEntryPackageVersion>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryPackageVersionsValidateBeforeCall(providerId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackageVersion>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
